#-------------------------------------------------
#
# Project created by QtCreator 2013-04-06T08:57:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
VERSION = 0.2

DEFINES += KLF_SRC_BUILD

TARGET = TinyTex
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

INCLUDEPATH += $$PWD/klfbackend
win32 {
    msvc:LIBS  += $$PWD/build-KLFBackend/KLFBackend.lib
    mingw:LIBS += $$PWD/build-KLFBackend/libKLFBackend.a
} else {
    LIBS += $$PWD/build-KLFBackend/libKLFBackend.a
}
